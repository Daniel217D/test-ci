<?php

declare(strict_types=1);

function sum(float $a, float $b): float
{
    return $a + $b;
}

function sub(float $a, float $b): float
{
    return $a - $b;
}
