#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -x
#set -e

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git -yqq
apt-get install zip -yqq
apt-get install unzip -yqq

# Here you can install any other extension that you need
#docker-php-ext-install ...

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
chmod +x composer-setup.php
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"
chmod +x /usr/local/bin/composer