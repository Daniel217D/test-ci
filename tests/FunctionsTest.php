<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

final class FunctionsTest extends TestCase
{
    public function testSum(): void {
        $this->assertEquals(4, sum(2,2));
    }

    public function testSub(): void {
        $this->assertEquals(2, sub(4,2));
    }
}